

// this is an example that  creat a service_client  requesting a spawn

#include"ros/ros.h"
#include"turtlesim/Spawn.h"

int main(int argc, char *argv[])
{
    /* code for main function */
    ros::init(argc, argv, "spawn_service");
    
    ros::NodeHandle node;

    ros::ServiceClient spawnClient = node.serviceClient <turtlesim::Spawn>("spawn");

    turtlesim::Spawn::Request req;
    turtlesim::Spawn::Response resp;

    req.x =2;
    req.y =3;
    req.theta = M_PI/2;
    req.name = "HUA";

    bool success = spawnClient.call(req,resp);

    if(success)
        {
            ROS_INFO_STREAM("spawn_a_turtle_named "<<resp.name);
        }
    else
    {
        ROS_INFO("failed to spawn ");
    }
    return 0;
}