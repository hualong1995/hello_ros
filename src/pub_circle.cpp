
#include<ros/ros.h>
#include<geometry_msgs/Twist.h>

int main(int argc, char *argv[])
{
ros::init(argc, argv, "circle");

ros::NodeHandle na;

ros::Publisher pub = na.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 1000);

ros::Rate rate(2);  //频率

while (ros::ok())
{
    /* code for loop body */
    geometry_msgs::Twist msg;
    msg.linear.x = 4;
    msg.angular.z = 2.8;
    pub.publish(msg);

    rate.sleep();

}
return 0;
}



