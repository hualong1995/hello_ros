
// this is an example creat a service-server ,
//　run  the node of turtlesim_node
//  run this node toggle_sevice_server
//  through  call service-clent  /
//  rosservice call /toggle_forward   to control the turtle

#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "std_srvs/Empty.h"

    bool forward = true;  //全局变量，表示　行走/转向的开关
    bool toggleForward(         //回调函数，每次有服务请求，进入 [类似于中断服务函数]
        std_srvs::Empty::Request &req,
        std_srvs::Empty::Response &resp  //这里指定的都是空字符串
    ){
        forward = !forward;
        ROS_INFO_STREAM("Now sending :"<<(forward?"forward":"routate")<<"command.");
        return true;
    }


int main(int argc, char *argv[])
{
    /* code for main function */
    ros::init(argc, argv, "toggle_sevice_server");
    ros::NodeHandle node;

    //creat ros service-server
    ros::ServiceServer  server = node.advertiseService("toggle_forward",&toggleForward);

    //creat ros publisher
    ros::Publisher pub = node.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 1000);
   ros::Rate rate(200);
   while (ros::ok())
   {
       /* code for loop body */
       geometry_msgs::Twist msg;
       msg.linear.x = forward ? 1.0:0.0;
       msg.angular.z = forward ? 0.0:1.0;
       pub.publish(msg);
       ros::spinOnce();  //给控制权，轮训服务　类似于订阅节点

       rate.sleep();

   }
   


    return 0;
}